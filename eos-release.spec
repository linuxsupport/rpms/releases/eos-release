%define product eos
%define debug_package %{nil}

Name:		%{product}-release
Version:	1.0
Release:	1%{?dist}
Summary:	CERN %{product} repository release file
Group:		System Environment/Base
License:	GPLv2
URL:		http://linux.cern.ch/
BuildArch:  noarch
Source0:	%{product}.repo

%description
The package contains yum configuration for the CERN %{product} repository

%prep
%setup -q  -c -T
install -pm 0644 %{SOURCE0} .

%build

%install
rm -rf $RPM_BUILD_ROOT
install -dm 0755 $RPM_BUILD_ROOT%{_sysconfdir}/yum.repos.d
install -pm 0644 %{SOURCE0} $RPM_BUILD_ROOT%{_sysconfdir}/yum.repos.d

%files
%defattr(-,root,root,-)
%doc
%config(noreplace) /etc/yum.repos.d/*

%changelog
* Fri Nov 22 2019 Ben Morrice <ben.morrice@cern.ch> 1.0-1.el8.cern
- Initial release
